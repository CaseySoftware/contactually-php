<?php

namespace Contactually;

class ContactHistories extends \Contactually\Resources\Base
{
    protected $resource = 'contact_histories';
    protected $dataname = 'email_histories';

    /**
     * @todo
     */
    public function bulk_create() { }
}