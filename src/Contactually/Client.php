<?php

namespace Contactually;

use GuzzleHttp\Client as GuzzleClient;
use Contactually\Exceptions\InvalidResource;

/**
 * Class Client
 * @package Contactually
 *
 * @property-read string $accounts
 * @property-read string $contacts
 * @property-read string $contact_histories
 * @property-read string $email_aliases
 * @property-read string $email_templates
 * @property-read string $followups
 * @property-read string $groupings
 * @property-read string $notes
 * @property-read string $tasks
 */
class Client
{
    const USER_AGENT = 'contactually-php/2.0.0';

    protected $baseURI  = 'https://www.contactually.com/api/v1/';
    protected $apikey   = '';
    protected $client   = null;
    protected $request  = null;

    public $response = null;
    public $statusCode = null;
    public $detail   = null;

    /**
     * @param $apikey
     * @param null $httpClient
     */
    public function __construct($apikey, $httpClient = null)
    {
        $this->apikey = $apikey;
        $this->client = (is_null($httpClient)) ? new GuzzleClient(
            ['base_uri' => $this->baseURI, 'headers' => ['User-Agent' => $this::USER_AGENT . '/' . PHP_VERSION ]]
        ) : $httpClient;
    }

    /**
     * @param $uri
     * @param array $params
     * @return null
     */
    public function get($uri, $params = array())
    {
        $params['api_key'] = $this->apikey;

        $this->response = $this->client->get($uri, ['exceptions' => false, 'query' => $params] );
        $this->statusCode = $this->response->getStatusCode();
        $raw_body = $this->response->getBody();

        return json_decode($raw_body, true);
    }

    /**
     * @param $uri
     * @param array $params
     * @return mixed
     */
    public function put($uri, $params = array())
    {
        $params['api_key'] = $this->apikey;

        $this->response = $this->client->put($uri, ['exceptions' => false, 'query' => $params]);
        $this->statusCode = $this->response->getStatusCode();

        return $this->isSuccessful();
    }

    /**
     * @param $uri
     * @param array $params
     * @return mixed
     */
    public function post($uri, $params = array())
    {
        $params['api_key'] = $this->apikey;

        $this->response = $this->client->post($uri, ['exceptions' => false, 'query' => $params]);
        $this->statusCode = $this->response->getStatusCode();

        return $this->isSuccessful();
    }

    /**
     * @param $uri
     * @return bool
     */
    public function delete($uri)
    {
        $params['api_key'] = $this->apikey;

        $this->response = $this->client->delete($uri, ['exceptions' => false, 'query' => $params]);
        $this->statusCode = $this->response->getStatusCode();

        return $this->isSuccessful();
    }

    protected function processRequest($request)
    {
        $this->response = $request->send();
        $this->statusCode = $this->response->getStatusCode();
        $this->detail = $this->response->json();
    }

    protected function addFields($request, $params = array())
    {
        $params['api_key'] = $this->apikey;
        foreach($params as $key => $value) {
            $request->getQuery()->set($key, $value);
        }
        return $request;
    }

    protected function isSuccessful()
    {
        return ('2' == substr($this->statusCode, 0 , 1));
    }

    /**
     * @param $name
     * @return Accounts|ContactHistories|Contacts|EmailAliases|EmailTempaltes|Followups|Groupings|Notes|Tasks
     * @throws Exceptions\InvalidResourceException
     */
    public function __get($name)
    {
        $classname = ucwords(str_replace("_", " ", $name));
        $fullclass = "Contactually\\" . str_replace(' ', '', $classname);

        if (class_exists($fullclass)) {
            return new $fullclass($this);
        }

        throw new \Contactually\Exceptions\InvalidResource('Not supported');
    }
}
